package frieze;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Cell {
	private BufferedImage cell;

	public Cell() {
		try {
			cell = ImageIO.read(new File("image.jpg"));
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public Cell(Cell c) {
		cell = c.cell;
	}
	
	public Cell(String fname){
		try{
			cell = ImageIO.read(new File(fname));
		} catch(IOException ex){
			ex.printStackTrace();
		}
	}
	
	public BufferedImage getCellImage(){
		return cell;
	}

	public Cell verticalReflection() {
		Cell result = new Cell();
		AffineTransform at = AffineTransform.getScaleInstance(1d, -1d);
		at.translate(0, -cell.getHeight());
		AffineTransformOp op = new AffineTransformOp(at,
				AffineTransformOp.TYPE_NEAREST_NEIGHBOR);

		result.cell = op.filter(cell, null);

		return result;
	}

	public Cell horizontalReflection() {
		Cell result = new Cell();
		AffineTransform at = AffineTransform.getScaleInstance(-1d, 1d);
		at.translate(-cell.getWidth(), 0);
		AffineTransformOp op = new AffineTransformOp(at,
				AffineTransformOp.TYPE_NEAREST_NEIGHBOR);

		result.cell = op.filter(cell, null);

		return result;
	}
	
	public Cell rotation(double theta){
		Cell result =  new Cell();
		AffineTransform at = AffineTransform.getScaleInstance(-1d, 1d);
		at.rotate(theta, cell.getWidth(), cell.getHeight());
		AffineTransformOp op = new AffineTransformOp(at,
				AffineTransformOp.TYPE_NEAREST_NEIGHBOR);

		result.cell = op.filter(cell, null);
		
		return result;
	}
}
