package frieze;

import java.awt.image.BufferedImage;

import javax.swing.JPanel;

import java.util.ArrayList;
import java.util.List;

public class Frieze extends JPanel {
	
	protected List<Cell> frieze = new ArrayList<Cell>();
	
	public Frieze() {
		Cell c = new Cell("image.jpg");
		frieze.add(c);
		frieze.add(c.verticalReflection());
	}

	public BufferedImage getFriezeImage(){
		BufferedImage result = frieze.get(0).getCellImage();
		
		for(Cell c: frieze.subList(1, frieze.size())){
			result = concatImage(result, c.getCellImage());
		}
		
		
		return result;
	}

	private BufferedImage concatImage(BufferedImage left, BufferedImage right) {
		int lwidth = left.getWidth();
		int lheight = left.getHeight();
		int rwidth = right.getWidth();
		int rheight = right.getHeight();

		BufferedImage result = new BufferedImage(lwidth + rwidth, 
				Math.max(lheight, rheight), BufferedImage.TYPE_INT_RGB);

		if (!result.createGraphics().drawImage(left, 0, 0, null)
				|| !result.createGraphics().drawImage(right, lwidth, 0, null)) {
			throw new RuntimeException("Unable to concatenate image");
		}

		return result;
	}

	
}